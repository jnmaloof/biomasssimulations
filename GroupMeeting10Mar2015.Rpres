```{r setup, include=FALSE}
opts_chunk$set(cache=TRUE)
```

Biomass QTL Simulations
====================================
author: Julin Maloof
date: March 10, 2015
 Or: simulating the obvious

Introduction: ARPA-E Grant
====================================
incremental: true
* Program Goal: Improve and accelerate breeding of Energy Sorghum
* Typically biomass is a hard trait to breed for
* Target: Find loci that explain > 70% of genetic variation in biomass

Genetic Variance
================
incremental: true
* If we think about a natural population of plants (or a QTL mapping population):
* Individual variation
* Some variation is environmental 
* Some variation is genetic
* We can partition the variance by ANOVA
  * Variance caused by the environment / error (VarE)
  * Variance caused by genetic differences (VarG)
  * Does not depend on being able to map the causative loci
  
Mapping the Loci
================
incremental: true
* For Marker Assisted breeding or biotech we need to map the loci controlling variance
* This can be done by a Genome Wide Association Study (GWAS) or by QTL mapping
* Typically the biomass loci found by these techniques don't explain very much of VarG

Back to biomass
===============
incremental: true
* Why is biomass a hard trait to breed for?
* Very complex trait
  * Many different "paths" to high biomass, e.g. 
      * A few large stems or many small stems
      * Grow early, grow late
      * leaf position and angle
  * A recent paper found independent genetic control for biomass traits in Rice.
  
The Idea
========
* The causative traits for biomass (leaf angle, architecture, etc) are removed from biomass
* Map QTL for the biomass component traits rather than for biomass itself

The (Very Simplistic) Simulation
==============
* Population of 200
* 8 component traits
* Each component trait is controlled by 5 QTL
* Biomass is simply the sum of the effects of the component traits
* Run it 10 or 100 times

Example FDR 100 Markers
===========
![simulation](simulations2_nofudge100.png)

Example FDR 10000 Markers
===========
![simulation](simulations2_nofudge10000.png)

Comments/Problems
=================
incremental: true
* It is actually even harder to detect biomass QTL than shown on the previous slide
  * I fudged the FDR adjustment
* Maybe this is stupid.
  * Each component trait is controlled by 5 QTL
  * Biomass is controlled by 40 QTL 1/8 the size of component QTL
  * Small QTL are harder to map than QTL
* So no simulation was needed.  
* But it does illustrate the point
* Epistasis might be more interesting


  


